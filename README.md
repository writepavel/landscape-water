# landscape-water


Hello!

This is a little algorithm project.


- UI made with kotlin + tornadoFX
- Tests for the algorithm made with JUnit5
- Algorithm made with java, in `com.pavel.task.service.TaskSolveAlgorithm` file.

## How To Run Application

```
mvn package
java -jar target\landscape-water-all.jar
```


Best regards, Pavel