package com.pavel.task.service;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class HeightsConverterTest {

    private HeightsConverter converter = new HeightsConverter();

    private static Stream<Arguments> createStringToArrayData() {
        return Stream.of(
                Arguments.of("1,2,3", new int[]{1,2,3}),
                Arguments.of("2,,2", new int[]{2,0,2}));
    }

    @ParameterizedTest
    @MethodSource(value = "createStringToArrayData")
    void testFromStringToArray(String heightsStr, Object heightsArray) {
        int[] heightsArrayResult = converter.convertStringToArray(heightsStr);
        assertTrue(Arrays.equals((int[]) heightsArray, heightsArrayResult));
    }

    private static Stream<Arguments> createArrayToStringData() {
        return Stream.of(
                Arguments.of(new int[]{1,2,3}, "1,2,3"),
                Arguments.of(new int[]{2,0,2}, "2,0,2"));
    }

    @ParameterizedTest
    @MethodSource(value = "createArrayToStringData")
    void testFromArrayToString(Object heightsArray, String heightsStr) {
        String heightsStringResult = converter.convertArrayToString((int[]) heightsArray);
        assertEquals(heightsStr, heightsStringResult);
    }
}