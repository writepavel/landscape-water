package com.pavel.task.service;

import com.pavel.task.model.ImmutableValues;
import com.pavel.task.model.IndexBounds;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.time.Duration;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TaskSolveAlgorithmTest {

    private TaskSolveAlgorithm taskSolveAlgorithm = new TaskSolveAlgorithm();

    @ParameterizedTest
    @MethodSource(value = "createArrayToNavigableMapData")
    void testGetSortedHeights(Object heights, NavigableMap<Integer, IndexBounds> sortedHeights) {
        NavigableMap<Integer, IndexBounds> sortedHeightrsResult = taskSolveAlgorithm.getSortedHeights((int[]) heights);
        assertEquals(sortedHeights, sortedHeightrsResult);
    }

    @ParameterizedTest
    @MethodSource(value = "createArrayToResultData")
    void testGetResult(Object heights, ImmutableValues result) {
        ImmutableValues countedResult = taskSolveAlgorithm.solveTask((int[]) heights);
        assertEquals(result, countedResult);
    }

    @ParameterizedTest
    @MethodSource(value = "createPourFromRightStepData")
    void testPourFromRight(Object stepData, int pouredFromRight) {
        int[] values = (int[]) stepData;
        int countedResult = taskSolveAlgorithm.pourFromRight(values[0], values[1], values[2], values[3], values[4]);
        assertEquals(pouredFromRight, countedResult);
    }

    @ParameterizedTest
    @MethodSource(value = "createPourFromLeftStepData")
    void testPourFromLeft(Object stepData, int pouredFromLeft) {
        int[] values = (int[]) stepData;
        int countedResult = taskSolveAlgorithm.pourFromLeft(values[0], values[1], values[2], values[3], values[4]);
        assertEquals(pouredFromLeft, countedResult);
    }

    // int pourFromRight(int previousHeight, int prevRightMostIndex, int nextHeight, int nextRightMostIndex, int arrayLength) {
    @SuppressWarnings("unchecked")
    private static Stream<Arguments> createPourFromRightStepData() {
        int[] heights1 = {1, 2, 3, 2};
        int[] heightStep1 = {3, 2, 2, 3, 4};
        int pouredFromRight1 = 1;

        int[] heights2 = {1,3,2,3};
        int[] heightStep2 = {3, 3, 2, 2, 4};
        int pouredFromRight2 = 0;

        return Stream.of(
                Arguments.of(heightStep1, pouredFromRight1),
                Arguments.of(heightStep2, pouredFromRight2));
    }

    //int pourFromLeft(int previousHeight, int prevLeftMostIndex, int nextHeight, int nextLeftMostIndex, int arrayLength) {
        @SuppressWarnings("unchecked")
    private static Stream<Arguments> createPourFromLeftStepData() {
        int[] heights1 = {1, 2, 3, 2};
        int[] heightStep1 = {3, 2, 2, 1, 4};
        int pouredFromLeft1 = 2;

        int[] heights2 = {1,3,2,3};
        int[] heightStep2 = {3, 1, 2, 2, 4};
        int pouredFromLeft2 = 0;

        return Stream.of(
                Arguments.of(heightStep1, pouredFromLeft1),
                Arguments.of(heightStep2, pouredFromLeft2));
    }

    @SuppressWarnings("unchecked")
    private static Stream<Arguments> createArrayToResultData() {
        int[] heights1 = {1,2,3,2};
        ImmutableValues result1 = new ImmutableValues(
                "",
                12,
                4,
                3,
                1,
                0,
                Duration.ZERO);

        int[] heights2 = {1,3,2,3};
        ImmutableValues result2 = new ImmutableValues(
                "",
                12,
                3,
                2,
                0,
                1,
                Duration.ZERO);

        int[] heights3 = {2,7,7,4,1,0,8,6};
        ImmutableValues result3 = new ImmutableValues(
                "",
                64,
                29,
                11,
                2,
                16,
                Duration.ZERO);

        return Stream.of(
                Arguments.of(heights1, result1),
                Arguments.of(heights2, result2),
                Arguments.of(heights3, result3)
        );

    }

    @SuppressWarnings("unchecked")
    private static Stream<Arguments> createArrayToNavigableMapData() {
        int[] heights1 = {3,4,1,2};
        NavigableMap<Integer, IndexBounds> sortedHeights1 = new TreeMap<Integer, IndexBounds>((o1, o2) -> -o1.compareTo(o2)) {{
            put(4,IndexBounds.of(1));
            put(3,IndexBounds.of(0));
            put(2,IndexBounds.of(3));
            put(1,IndexBounds.of(2));
        }};

        int[] heights2 = {1,4,1,2, 3, 1, 3, 4, 3};
        NavigableMap<Integer, IndexBounds> sortedHeights2 = new TreeMap() {{
            put(4,IndexBounds.of(1, 7));
            put(3,IndexBounds.of(4, 8));
            put(2,IndexBounds.of(3));
            put(1,IndexBounds.of(0, 5));
        }};
        return Stream.of(
                Arguments.of(heights1, sortedHeights1),
                Arguments.of(heights2, sortedHeights2));
    }
}