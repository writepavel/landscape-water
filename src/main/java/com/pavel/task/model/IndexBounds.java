package com.pavel.task.model;

import java.util.Objects;

public class IndexBounds {

    private IndexBounds(int leftMostIndex, int rightMostIndex) {
        this.leftMostIndex = leftMostIndex;
        this.rightMostIndex = rightMostIndex;
    }

    public int getLeftMostIndex() {
        return leftMostIndex;
    }

    public void setLeftMostIndex(int leftMostIndex) {
        this.leftMostIndex = leftMostIndex;
    }

    public int getRightMostIndex() {
        return rightMostIndex;
    }

    public void setRightMostIndex(int rightMostIndex) {
        this.rightMostIndex = rightMostIndex;
    }

    private int leftMostIndex;
    private int rightMostIndex;

    public static IndexBounds of(int singleValue) {
        return new IndexBounds(singleValue, singleValue);
    }

    public static IndexBounds of(int leftMostIndex, int rightMostIndex) {
        return new IndexBounds(leftMostIndex, rightMostIndex);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexBounds that = (IndexBounds) o;
        return leftMostIndex == that.leftMostIndex &&
                rightMostIndex == that.rightMostIndex;
    }

    @Override
    public String toString() {
        return "IndexBounds{" +
                leftMostIndex +
                ", " + rightMostIndex +
                '}';
    }

    @Override
    public int hashCode() {
        return Objects.hash(leftMostIndex, rightMostIndex);
    }
}
