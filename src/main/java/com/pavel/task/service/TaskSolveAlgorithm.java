package com.pavel.task.service;

import com.pavel.task.model.ImmutableValues;
import com.pavel.task.model.IndexBounds;
import org.jetbrains.annotations.NotNull;
import tornadofx.Controller;

import java.time.Duration;
import java.util.*;

public class TaskSolveAlgorithm extends Controller {

    private final Comparator<Integer> reverseComparator = (o1, o2) -> -o1.compareTo(o2);

    public ImmutableValues solveTask(int[] heights) {
        final int length = heights.length;
        final int maxHeight = Arrays.stream(heights).max().getAsInt();
        final int maxCapacity = length * maxHeight;
        final int heightSum = Arrays.stream(heights).sum();
        final int maxWaterCapacity = maxCapacity - heightSum;
        int pouredFromLeft = 0;
        int pouredFromRight = 0;
        int[] leftEdgeIndex = {length - 1};
        int[] rightEdgeIndex = {0};
        NavigableMap<Integer, IndexBounds> sortedHeights = getSortedHeights(heights);
        System.out.println("map = " + sortedHeights);

        Map.Entry<Integer, IndexBounds> prevHeightEntry = sortedHeights.firstEntry();
        Map.Entry<Integer, IndexBounds> nextHeightEntry = sortedHeights.higherEntry(prevHeightEntry.getKey());

        while(nextHeightEntry != null) {
            pouredFromLeft = pouredFromLeft + pourFromLeft(prevHeightEntry, nextHeightEntry, heights, leftEdgeIndex);
            pouredFromRight = pouredFromRight + pourFromRight(prevHeightEntry, nextHeightEntry, heights, rightEdgeIndex);
            prevHeightEntry = nextHeightEntry;
            nextHeightEntry = sortedHeights.higherEntry(prevHeightEntry.getKey());
        }

        final int savedWater = maxWaterCapacity - pouredFromLeft - pouredFromRight;

        return new ImmutableValues("", maxCapacity, maxWaterCapacity, pouredFromLeft,pouredFromRight, savedWater, Duration.ZERO );
    }

    private int pourFromRight(Map.Entry<Integer, IndexBounds> prevHeightEntry, Map.Entry<Integer, IndexBounds> nextHeightEntry, int[] heights, int[] rightEdgeIndex) {
        int prevRightMostIndex = prevHeightEntry.getValue().getRightMostIndex();
        int nextHeight = nextHeightEntry.getKey();
        int nextRightMostIndex = nextHeightEntry.getValue().getRightMostIndex();
        rightEdgeIndex[0] = Math.max(rightEdgeIndex[0], prevRightMostIndex);
        int previousHeight = heights[rightEdgeIndex[0]];
        return  pourFromRight(previousHeight, rightEdgeIndex[0], nextHeight, nextRightMostIndex, heights.length);
    }

    int pourFromRight(int previousHeight, int rightEdgeIndex, int nextHeight, int nextRightMostIndex, int arrayLength) {

        if (nextRightMostIndex < rightEdgeIndex) {
            return 0;
        }
        int heightDelta = previousHeight - nextHeight;
        int lengthToTheRightEdge = arrayLength - rightEdgeIndex - 1;
        return heightDelta * lengthToTheRightEdge;
    }

    private int pourFromLeft(Map.Entry<Integer, IndexBounds> prevHeightEntry, Map.Entry<Integer, IndexBounds> nextHeightEntry, int[] heights, int[] leftEdgeIndex) {
        int prevLeftMostIndex = prevHeightEntry.getValue().getLeftMostIndex();
        int nextHeight = nextHeightEntry.getKey();
        int nextLeftMostIndex = nextHeightEntry.getValue().getLeftMostIndex();
        leftEdgeIndex[0] = Math.min(leftEdgeIndex[0], prevLeftMostIndex);
        int previousHeight = heights[leftEdgeIndex[0]];
        return pourFromLeft(previousHeight, leftEdgeIndex[0], nextHeight, nextLeftMostIndex, heights.length);
    }

    int pourFromLeft(int previousHeight, int leftEdgeIndex, int nextHeight, int nextLeftMostIndex, int arrayLength) {
        if (nextLeftMostIndex > leftEdgeIndex) {
            return 0;
        }
        int heightDelta = previousHeight - nextHeight;
        return heightDelta * leftEdgeIndex;
    }

    @NotNull
    NavigableMap<Integer, IndexBounds> getSortedHeights(int[] heights) {
        NavigableMap<Integer, IndexBounds> sortedHeights =  new TreeMap<>(reverseComparator);
        for (int i = 0; i < heights.length; i++) {
            int height = heights[i];
            IndexBounds bounds = sortedHeights.get(height);
            if (bounds == null) {
                sortedHeights.put(height, IndexBounds.of(i));
            } else {
                if (i < bounds.getLeftMostIndex()) {
                    bounds.setLeftMostIndex(i);
                }
                if (i > bounds.getRightMostIndex()) {
                    bounds.setRightMostIndex(i);
                }
            }
        }
        return sortedHeights;
    }
}
