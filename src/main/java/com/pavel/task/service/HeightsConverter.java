package com.pavel.task.service;

import tornadofx.Controller;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;

public class HeightsConverter extends Controller {

    public int[] convertStringToArray(String heightsStr) {
        return Arrays
                .stream(heightsStr.split("\\s*,\\s*"))
                .mapToInt(value -> value.length() == 0 ? 0 : Integer.valueOf(value))
                .toArray();
    }

    public String convertArrayToString (int[] heightsArray) {
        return Arrays
                .stream(heightsArray)
                .mapToObj(String::valueOf)
                .collect(Collectors.joining(","));
    }

    public int[] generateRandomHeights(int length, int maxHeight) {
        Random random= new Random();
        int heights[]= new int[length];
        for (int i = 0; i < length; i++)
        {
            int height= random.nextInt(maxHeight + 1);
            heights[i]=height;
        }
        return heights;
    }
}
