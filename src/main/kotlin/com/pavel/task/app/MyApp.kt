package com.pavel.task.app

import com.pavel.task.view.MainView
import tornadofx.*

class MyApp: App(MainView::class, Styles::class)