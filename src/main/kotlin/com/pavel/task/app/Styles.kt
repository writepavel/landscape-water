package com.pavel.task.app

import javafx.scene.text.FontWeight
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

class Styles : Stylesheet() {
    companion object {
        val heading by cssclass()
    }

    init {
        s(form) {
            padding = box(25.px)
            prefWidth = 450.px

        }
        label and heading {
            padding = box(10.px)
            fontSize = 10.px
            fontWeight = FontWeight.BOLD
        }
    }
}