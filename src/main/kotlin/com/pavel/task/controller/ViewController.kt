package com.pavel.task.controller

import com.pavel.task.model.ImmutableValues
import com.pavel.task.service.HeightsConverter
import com.pavel.task.service.TaskSolveAlgorithm
import tornadofx.*
import java.time.Duration
import java.time.LocalDateTime

class ViewController : Controller() {

    val heightsConverter : HeightsConverter by inject()
    val taskSolveAlgorithm : TaskSolveAlgorithm by inject()

    fun countValues(model: ImmutableValues): ImmutableValues {
        val heights : IntArray = heightsConverter.convertStringToArray(model.hightsInput)
        val now1 = LocalDateTime.now()
        val result : ImmutableValues = taskSolveAlgorithm.solveTask(heights)
        val now2 = LocalDateTime.now()
        val takenTime = Duration.between(now1, now2)
        return result.copy(hightsInput = model.hightsInput, takenTime = takenTime)
    }

    fun generateRandomHeights(length: Int, maxHeight: Int) : String {
        val heights : IntArray = heightsConverter.generateRandomHeights(length, maxHeight)
        return heightsConverter.convertArrayToString(heights)
    }
}