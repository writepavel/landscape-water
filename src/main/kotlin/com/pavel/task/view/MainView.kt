package com.pavel.task.view

import com.pavel.task.controller.ViewController
import com.pavel.task.model.TaskDataModel
import com.pavel.task.model.maxHeightLength
import javafx.scene.control.TextFormatter
import javafx.scene.control.TextInputControl
import tornadofx.*
import java.util.function.UnaryOperator

class MainView : View("Landscape Water Test") {

    val model : TaskDataModel by inject()
    val controller : ViewController by inject()

    fun TextInputControl.filterInput(discriminator: (TextFormatter.Change) -> Boolean) {
        textFormatter = TextFormatter<Any>(CustomTextFilter(discriminator))
    }

    override val root = form {

        fieldset("Enter heights or generate random:") {
            vbox {
                hbox {
                    label("Heights:")
                    spacing = 10.00
                    textfield(model.hightsInput) {
                        minWidth = 330.00
                        required()
                        filterInput {  it.controlNewText.matches(Regex("[0-9]+[,[0-9]+]*"))}
                    }
                }
                spacing = 10.00
                hbox {
                    label("Heights length:")
                    spacing = 10.00
                    textfield(model.randomInputLength) {
                        maxWidth = 50.00
                        filterInput { it.controlNewText.matches(Regex("[0-9]+")) }
                    }
                    spacing = 10.00
                    label("max height:")
                    spacing = 10.00
                    textfield(model.randomInputMaxHeight){
                        maxWidth = 50.00
                        filterInput { it.controlNewText.matches(Regex("[0-9]+")) }
                    }
                    button("random heights") {
                        action {
                            runAsync {
                                controller.generateRandomHeights(model.randomInputLength.value, model.randomInputMaxHeight.value)
                            } ui { randomHeightsStr ->
                                model.hightsInput.value = randomHeightsStr
                                model.commit()
                            }
                        enableWhen(model.randomInputLength.greaterThan(0)
                                .and(model.randomInputMaxHeight.greaterThan(0))
                                .and(model.randomInputMaxHeight.lessThan(com.pavel.task.model.maxHeight))
                                .and(model.randomInputLength.lessThan(maxHeightLength)))
                    }
                }
            }
        }

        fieldset("Calculate saved water capacity:") {
            button("Calculate"){
                action {
                    runAsync {
                        model.commit()
                        controller.countValues(model.exportToValues())
                    } ui { countedData ->
                        model.importFromValues(countedData)
                    }

//                    model.commit()
                }
                enableWhen(model.valid)
            }
        }

        fieldset("In-between results:") {
            vbox{
                field("max capacity (max height x array length): ") {
                    label(model.maxCapacity)
                }
                field("max water capacity (max capacity - heights sum): ") {
                    label(model.maxWaterCapacity)
                }
                field("poured water from left edge: ") {
                    label(model.pouredFromLeftEdge)
                }
                field("poured water from right edge:  ") {
                    label(model.pouredFromRightEdge)
                }
            }
        }

        fieldset("Result:") {
            field("Saved water capacity:  ") {
                label(model.savedWater)
            }
            field("Taken time to solve:  ") {
                label(model.takenTime)
            }
        }
    }
}

class CustomTextFilter(private val discriminator: (TextFormatter.Change) -> Boolean) : UnaryOperator<TextFormatter.Change> {
    override fun apply(t: TextFormatter.Change): TextFormatter.Change {
        if (!discriminator(t)) {
            t.text = ""
        }
        return t
    }
}
}
