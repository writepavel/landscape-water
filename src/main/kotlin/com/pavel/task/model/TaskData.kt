package com.pavel.task.model

import javafx.beans.property.IntegerProperty
import javafx.beans.property.IntegerProperty.integerProperty
import javafx.beans.property.Property
import javafx.beans.property.StringProperty
import tornadofx.*
import java.time.Duration

val maxHeight = 32000
val maxHeightLength = Integer.MAX_VALUE.div(maxHeight)

data class ImmutableValues (val hightsInput:String,
                            val maxCapacity:Int,
                            val maxWaterCapacity: Int,
                            val pouredFromLeftEdge : Int,
                            val pouredFromRightEdge : Int,
                            val savedWater: Int,
                            val takenTime: Duration)

class TaskPropertyData {

    var hightsInput: String by property<String>()
    fun hightsInputProperty() = getProperty(TaskPropertyData::hightsInput)

    var randomInputLength: Int by property<Int>()
    fun randomInputLengthProperty(): IntegerProperty = integerProperty(getProperty(TaskPropertyData::randomInputLength))

    var randomInputMaxHeight: Int by property<Int>()
    fun randomInputMaxHeightProperty(): IntegerProperty = integerProperty(getProperty(TaskPropertyData::randomInputMaxHeight))

    var maxCapacity: Int by property<Int>()
    fun maxCapacityProperty(): IntegerProperty = integerProperty(getProperty(TaskPropertyData::maxCapacity))

    var maxWaterCapacity: Int by property<Int>()
    fun maxWaterCapacityProperty(): IntegerProperty = integerProperty(getProperty(TaskPropertyData::maxWaterCapacity))

    var pouredFromLeftEdge: Int by property<Int>()
    fun pouredFromLeftEdgeProperty(): IntegerProperty = integerProperty(getProperty(TaskPropertyData::pouredFromLeftEdge))

    var pouredFromRightEdge: Int by property<Int>()
    fun pouredFromRightEdgeProperty(): IntegerProperty = integerProperty(getProperty(TaskPropertyData::pouredFromRightEdge))

    var savedWater: Int by property<Int>()
    fun savedWaterProperty(): IntegerProperty = integerProperty(getProperty(TaskPropertyData::savedWater))

    var takenTime: Duration by property<Duration>()
    fun takenTimeProperty() = getProperty(TaskPropertyData::takenTime)

    init {
        takenTime = Duration.ZERO
    }
}

class TaskDataModel : ItemViewModel<TaskPropertyData>(TaskPropertyData()) {
    val hightsInput: StringProperty = bind { item?.hightsInputProperty() }
    val randomInputLength: IntegerProperty = bind { item?.randomInputLengthProperty() }
    val randomInputMaxHeight: IntegerProperty = bind { item?.randomInputMaxHeightProperty() }
    val maxCapacity: IntegerProperty = bind { item?.maxCapacityProperty() }
    val maxWaterCapacity: IntegerProperty = bind { item?.maxWaterCapacityProperty() }
    val pouredFromLeftEdge: IntegerProperty = bind { item?.pouredFromLeftEdgeProperty() }
    val pouredFromRightEdge: IntegerProperty = bind { item?.pouredFromRightEdgeProperty() }
    val savedWater: IntegerProperty = bind { item?.savedWaterProperty() }
    val takenTime: Property<Duration> = bind { item?.takenTimeProperty() }

    fun exportToValues():ImmutableValues {
        return ImmutableValues(hightsInput.value, maxCapacity.value, maxWaterCapacity.value, pouredFromLeftEdge.value, pouredFromRightEdge.value, savedWater.value, takenTime.value)
    }

    fun importFromValues(data: ImmutableValues) {
        hightsInput.value = data.hightsInput
        maxCapacity.value = data.maxCapacity
        maxWaterCapacity.value = data.maxWaterCapacity
        pouredFromLeftEdge.value = data.pouredFromLeftEdge
        pouredFromRightEdge.value = data.pouredFromRightEdge
        savedWater.value = data.savedWater
        takenTime.value = data.takenTime
    }
}
